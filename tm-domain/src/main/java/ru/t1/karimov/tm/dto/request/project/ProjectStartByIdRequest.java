package ru.t1.karimov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectStartByIdRequest(@Nullable final String token) {
        super(token);
    }

}
