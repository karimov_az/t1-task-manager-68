package ru.t1.karimov.tm.dto.response.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.karimov.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class DataBackupLoadResponse extends AbstractResponse {
}
