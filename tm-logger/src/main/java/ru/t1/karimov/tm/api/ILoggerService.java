package ru.t1.karimov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public interface ILoggerService {
    @SneakyThrows
    void log(@NotNull String json);
}
