package ru.t1.karimov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.dto.model.ProjectDto;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

}
