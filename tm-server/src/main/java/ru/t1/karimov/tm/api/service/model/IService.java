package ru.t1.karimov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    boolean existsById(@Nullable String id);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    Long getSize();

    void removeAll();

    void removeOne(M model);

    void removeOneById(@Nullable String id);

    void removeOneByIndex(@Nullable Integer index);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void update(M model);

}
