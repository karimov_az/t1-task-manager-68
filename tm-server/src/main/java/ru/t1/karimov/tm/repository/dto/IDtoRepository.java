package ru.t1.karimov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;

@NoRepositoryBean
public interface IDtoRepository<M extends AbstractDtoModel> extends JpaRepository<M, String> {

}

