package ru.t1.karimov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
