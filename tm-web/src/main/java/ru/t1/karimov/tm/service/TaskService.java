package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    private static final String ID_EMPTY = "Id is empty";

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void update(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException(ID_EMPTY);
        taskRepository.deleteById(id);
    }

    @Nullable
    public Task findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException(ID_EMPTY);
        return taskRepository.findById(id).orElse(null);
    }

}
