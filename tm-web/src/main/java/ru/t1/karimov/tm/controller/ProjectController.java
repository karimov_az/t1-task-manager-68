package ru.t1.karimov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.ProjectService;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectService.add(new Project("New Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
        return "redirect:/projects";
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") final Project project,
            BindingResult result
    ) {
        projectService.add(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) {
        @Nullable final Project project = projectService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
