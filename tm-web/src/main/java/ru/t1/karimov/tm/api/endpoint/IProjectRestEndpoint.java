package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.karimov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("api/projects")
public interface IProjectRestEndpoint {

    @NotNull
    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("findById/{id}")
    Project findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @PostMapping("delete/{id}")
    @WebMethod(operationName = "deleteById")
    void delete(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

}
