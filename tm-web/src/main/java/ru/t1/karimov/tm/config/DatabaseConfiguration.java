package ru.t1.karimov.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.t1.karimov.tm.api.repository")
public class DatabaseConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @Value("#{environment['database.driver']}") final String databaseDriver,
            @Value("#{environment['database.url']}") final String databaseUrl,
            @Value("#{environment['database.username']}") final String databaseUser,
            @Value("#{environment['database.password']}") final String databasePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("#{environment['database.sql_dialect']}") final String databaseDialect,
            @Value("#{environment['database.hbm2ddl_auto']}") final String databaseHbm2ddlAuto,
            @Value("#{environment['database.show_sql']}") final String databaseShowSql,
            @Value("#{environment['database.format_sql']}") final String databaseFormatSql,
            @Value("#{environment['database.comment_sql']}") final String databaseCommentsSql,
            @Value("#{environment['database.second_lvl_cache']}") final String databaseSecondLvlCache,
            @Value("#{environment['database.factory_class']}") final String databaseFactoryClass,
            @Value("#{environment['database.use_query_cache']}") final String databaseUseQueryCache,
            @Value("#{environment['database.use_min_puts']}") final String databaseUseMinPuts,
            @Value("#{environment['database.region_prefix']}") final String databaseRegionPrefix,
            @Value("#{environment['database.config_file_path']}") final String databaseHazelConfig
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource);
        bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        bean.setPackagesToScan("ru.t1.karimov.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseDialect);
        properties.put(Environment.HBM2DDL_AUTO, databaseHbm2ddlAuto);
        properties.put(Environment.SHOW_SQL, databaseShowSql);
        properties.put(Environment.FORMAT_SQL, databaseFormatSql);
        properties.put(Environment.USE_SQL_COMMENTS, databaseCommentsSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseSecondLvlCache);
        properties.put(Environment.CACHE_REGION_FACTORY, databaseFactoryClass);
        properties.put(Environment.USE_QUERY_CACHE, databaseUseQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, databaseUseMinPuts);
        properties.put(Environment.CACHE_REGION_PREFIX, databaseRegionPrefix);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseHazelConfig);
        bean.setJpaProperties(properties);
        return bean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return jpaTransactionManager;
    }

}
