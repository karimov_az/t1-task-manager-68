package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.karimov.tm.dto.soap.*;
import ru.t1.karimov.tm.service.ProjectService;

@Endpoint
public class ProjectSoapEndpointImpl {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.karimov.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    private ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    private ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjects(projectService.findAll());
        return response;
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    private ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        response.setProject(projectService.findOneById(request.getId()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    private ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.add(request.getProject());
        @NotNull final ProjectSaveResponse response = new ProjectSaveResponse();
        response.setProject(request.getProject());
        return response;
    }

}
