package ru.t1.karimov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.service.ProjectService;
import ru.t1.karimov.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private Collection<Task> getTasks() {
        return taskService.findAll();
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", getTasks());
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
