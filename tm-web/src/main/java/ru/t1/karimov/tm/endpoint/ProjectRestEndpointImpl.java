package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.karimov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping("/delete")
    public void delete(
            @RequestBody final Project project
    ) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(
            @PathVariable("id") final String id
    ) {
        return projectService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(
            @RequestBody final Project project
    ) {
        projectService.add(project);
        return project;
    }

}
